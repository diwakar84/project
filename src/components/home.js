import React, { useState, useEffect } from "react";
import Country from "./country";
import { Link } from "react-router-dom";

function Home() {
  const [data, setdata] = useState([]);

  useEffect(() => {
    fetch(
      "https://coronavirus-monitor.p.rapidapi.com/coronavirus/worldstat.php",
      {
        mode: "cors",
        method: "GET",
        headers: {
          "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
          "x-rapidapi-key": "78133a533emsh2e5ae033415b9f4p1df913jsn928bdc7cc002"
        }
      }
    )
      .then(response => response.json())
      .then(resp => {
        setdata(resp);
        // console.log(resp);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container">
      {/* {console.log(data)} */}
      <Link to="/world">All countries</Link>
      <div className="card mt-3 mb-3 bg-primary">
        <h6 className="text-white">Around World</h6>
      </div>
      <div className="card-deck">
        <div className="card">
          <div className="card-body bg-info">
            <h5 className="card-title text-white">Total cases</h5>
            <h5 className="card-title text-white"> {data.total_cases}</h5>
          </div>
        </div>
        <div className="card bg-danger">
          <div className="card-body">
            <h5 className="card-title text-white">Total deaths</h5>
            <h5 className="card-title text-white">{data.total_deaths}</h5>
          </div>
        </div>

        <div className="card bg-success">
          <div className="card-body">
            <h5 className="card-title text-white">Total recovered</h5>
            <h5 className="card-title text-white">{data.total_recovered}</h5>
          </div>
        </div>
        <div className="card bg-info">
          <div className="card-body">
            <h5 className="card-title text-white">New cases</h5>
            <h5 className="card-title text-white">{data.new_cases}</h5>
          </div>
        </div>
      </div>
      <div className="card mt-3 bg-primary">
        <h6 className="text-white">
          Updated at-->
          {data.statistic_taken_at}
        </h6>
      </div>
      <Country />
    </div>
  );
}

export default Home;
