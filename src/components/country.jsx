import React, { useEffect, useState } from "react";
function Country() {
  const [country, setcountry] = useState([]);
  const [data, setdata] = useState([]);
  // useEffect(() => {
  //   fetch(
  //     "https://coronavirus-monitor.p.rapidapi.com/coronavirus/affected.php",
  //     {
  //       method: "GET",
  //       headers: {
  //         "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
  //         "x-rapidapi-key": "78133a533emsh2e5ae033415b9f4p1df913jsn928bdc7cc002"
  //       }
  //     }
  //   )
  //     .then(response => response.json())
  //     .then(resp => {
  //       setcountry(resp.affected_countries);
  //       // console.log(resp);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     });
  // }, []);
  useEffect(() => {
    fetch(
      "https://coronavirus-monitor.p.rapidapi.com/coronavirus/latest_stat_by_country.php?country=India",
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
          "x-rapidapi-key": "78133a533emsh2e5ae033415b9f4p1df913jsn928bdc7cc002"
        }
      }
    )
      .then(response => response.json())
      .then(resp => {
        // console.log(resp.latest_stat_by_country[0]);
        // console.log(Object.values(resp));
        setdata(resp.latest_stat_by_country[0]);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  let arr = country.length > 0 ? country.sort() : null;
  return (
    <div className="container">
      {/* <h5 className="text-info">Stats of India</h5> */}
      <div className="card mt-3 mb-3 bg-primary">
        <h6 className="text-white">{data.country_name}</h6>
      </div>
      {/* {console.log(arr)} */}
      {console.log(data)}

      <div className="card-deck">
        <div className="card">
          <div className="card-body bg-info">
            <h5 className="card-title text-white">Total cases</h5>
            <h5 className="card-title text-white"> {data.total_cases}</h5>
          </div>
        </div>
        <div className="card bg-success">
          <div className="card-body">
            <h5 className="card-title text-white">Total recovered</h5>
            <h5 className="card-title text-white">{data.total_recovered}</h5>
          </div>
        </div>
        <div className="card bg-danger">
          <div className="card-body">
            <h5 className="card-title text-white">Total deaths</h5>
            <h5 className="card-title text-white">{data.total_deaths}</h5>
          </div>
        </div>

        <div className="card bg-info">
          <div className="card-body">
            <h5 className="card-title text-white">New cases</h5>
            <h5 className="card-title text-white">{data.new_cases}</h5>
          </div>
        </div>
      </div>
      <div>
        <div className="card-deck">
          <div className="card bg-secondary mt-3">
            <div className="card-body">
              <h5 className="card-title text-white">Active cases</h5>
              <h5 className="card-title text-white">{data.active_cases}</h5>
            </div>
          </div>
          <div className="card bg-secondary mt-3">
            <div className="card-body">
              <h5 className="card-title text-white">Total Cases Per1m</h5>
              <h5 className="card-title text-white">
                {data.total_cases_per1m}
              </h5>
            </div>
          </div>
        </div>
      </div>
      <div className="card mt-3 bg-primary">
        <h6 className="text-white">
          Updated at-->
          {data.record_date}
        </h6>
      </div>
    </div>
  );
}
export default Country;
