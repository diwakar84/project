import React from "react";

import "./App.css";
import Home from "./components/home";
import Country from "./components/country";
import { BrowserRouter as Router, Route } from "react-router-dom";
import World from "./components/world";
function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Home} />
        <Route exactly path="/world" component={World} />
      </Router>
      <h5 className="text">
        DEVELOPED BY <span className="text-primary">Diwakar</span>
      </h5>
      <h5 className="text">
        Data Source{" "}
        <span className="text-primary">
          AVALON via
          <a href="https://rapidapi.com/astsiatsko/api/coronavirus-monitor/details">
            RapidApi
          </a>
        </span>
      </h5>
    </div>
  );
}

export default App;
