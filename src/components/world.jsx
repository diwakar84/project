import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Country() {
  const [datas, setdata] = useState([]);

  useEffect(() => {
    fetch(
      "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
          "x-rapidapi-key": "78133a533emsh2e5ae033415b9f4p1df913jsn928bdc7cc002"
        }
      }
    )
      .then(response => response.json())
      .then(resp => {
        setdata(resp.countries_stat);
        // console.log(resp);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container">
      {console.log(datas)}
      <Link to="/">Home</Link>

      {datas.length > 0
        ? datas.map((i, index) => {
            return (
              <div className="card p-3" key={index}>
                <div className="card-body bg-info">
                  <h5 className="card-title text-secondary">
                    {i.country_name}
                  </h5>
                  <h5 className="card-title text-white">
                    Total cases:{i.cases}
                  </h5>
                  <h5 className="card-title text-danger">
                    Total death:{i.deaths}
                  </h5>
                  <h5 className="card-title text-success">
                    Total recovered:{i.total_recovered}
                  </h5>
                  <h5 className="card-title text-secondary">
                    Serious critical:{i.serious_critical}
                  </h5>
                  <h5 className="card-title text-secondary">
                    Total cases per 1m population:
                    {i.total_cases_per_1m_population}
                  </h5>
                </div>
              </div>
            );
          })
        : "LOADING PLEASE WAIT....."}
    </div>
  );
}
export default Country;
